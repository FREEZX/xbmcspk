var Speakable = require('speakable');

var Xbmc = require('xbmc');

var connection = new Xbmc.TCPConnection({
    host: '127.0.0.1',
    port: 9090
});

var xbmcApi = new Xbmc.XbmcApi({
    connection: connection,
    debug: true
});

xbmcApi.on('connection:data', function(data)  { console.log('onData');  console.log(data)});
xbmcApi.on('connection:open', function()  { console.log('onOpen');  });
xbmcApi.on('connection:close', function() { console.log('onClose'); });



var speakable = new Speakable({lang: 'en-US', treshold: 0.1});


speakable.on('speechStart', function() {
    console.log('onSpeechStart');
});

speakable.on('speechStop', function() {
    console.log('onSpeechStop');
});

speakable.on('speechReady', function() {
    console.log('onSpeechReady');
});

speakable.on('error', function(err) {
    console.log('onError:');
    console.log(err);
    speakable.recordVoice();
});

speakable.on('speechResult', function(recognizedWords) {
    console.log('onSpeechResult:');
    console.log(recognizedWords);
    for(var i=0; i<recognizedWords.length; ++i){

        var word = recognizedWords[i].toLowerCase();
        if(word == 'youtube'){
            console.log('Playing youtube!');
            id = 'QH2-TGUlwu4';
            xbmcApi.player.openYoutube(id);
        }

        if(word == 'stop' || word=='top' || word=='bop'){
            // console.log(xbmcApi);
            xbmcApi.input.ExecuteAction('stop');
        }

        if(word == 'pause' || word == 'boss' || word == 'paws' || word == 'porn'){
            xbmcApi.input.ExecuteAction('pause');
        }

        if(word == 'up'){
            xbmcApi.input.ExecuteAction('up');
        }

        if(word == 'down'){
            xbmcApi.input.ExecuteAction('down');
        }

        if(word == 'left'){
            xbmcApi.input.ExecuteAction('left');
        }

        if(word == 'right'){
            xbmcApi.input.ExecuteAction('right');   
        }

        if(word == 'volume' && i+1<recognizedWords.length){
            if(recognizedWords[i+1] == 'up'){
                xbmcApi.input.ExecuteAction('volumeup');
            }
            else if(recognizedWords[i+1] == 'down'){
                xbmcApi.input.ExecuteAction('volumedown');
            }
            if(i+2 < recognizedWords.length){
                if(recognizedWords[i+2] == 'percent'){
                    var number = parseInt(recognizedWords[i+1]);
                    if(!isNaN(number)){
                        xbmcApi.input.
                    }
                }
            }
        }

        if(word == 'play'){
            // console.log(xbmcApi);
            xbmcApi.input.ExecuteAction('play');
        }

    }
    speakable.recordVoice();
});

speakable.recordVoice();
